# CloudFunction: Google Sheet Population

This project implements the deployment of a Terraform-managed and TypeScript-based Google Cloud Function (2nd gen) that continuously writes data from a Google Cloud SQL database to a Google Sheet.

The idea is to use Google Sheets with its powerful features as a web-based view layer for database data that can be flexibly shared with any Google user.

## Deployment

### Build Cloud Function
```bash
npm --prefix cloud-function run build
```

### Secrets
Add database password (e.g. `123123`) to `terraform/secrets.tfvars`:
```bash
cat <<EOT >> terraform/secrets.tfvars
db_password = "123123"
EOT
```

#### ⚠️ Warning
The password will be stored in plain text in your `terraform.tfstate` file.

For this reason `terraform.tfstate` is included in the file `.gitignore`, so that no secrets are versioned.

### Configuration
Populate the `config.tfvars` file with the project specific values:

```bash
cat <<EOT >> terraform/config.tfvars
db              = "db-01"
db_instance     = "db-instance-01"
db_user         = "user-01"
project         = "project-01"
region          = "europe-west3"
spreadsheet_id  = "3puYYKPd2z1w3d8Zv0bS"
EOT
```

### Initialization
This step is only needed once:

```bash
terraform -chdir=terraform init
```

### Infrastructure and Deployment
```bash
terraform \
-chdir=terraform \
apply \
-var-file="secrets.tfvars" \
-var-file="config.tfvars" \
-auto-approve
```

### Cloud SQL Connection

Once the function is deployed, the Cloud SQL connection has to be set manually on the underlying Cloud Run service as described in the [Google docs](https://cloud.google.com/sql/docs/mysql/connect-functions#configure).

Unfortunately this cannot be automated in Terraform as of today. There is [open issue](https://github.com/hashicorp/terraform-provider-google/issues/12578) on GitHub discussing this.

### Function Call
Replace `[function_name]` with the output value from terraform:

```bash
gcloud functions call [function_name] --gen2 --region europe-west3
```

## Cloud Function

### Obtain Scopes
```bash
gcloud auth application-default login --scopes=\
openid,\
https://www.googleapis.com/auth/userinfo.email,\
https://www.googleapis.com/auth/cloud-platform,\
https://www.googleapis.com/auth/sqlservice.login,\
https://www.googleapis.com/auth/accounts.reauth,\
https://www.googleapis.com/auth/spreadsheets
```

In addition to the default scopes, it's necessary to request the scope for accessing Google spreadsheets (`https://www.googleapis.com/auth/spreadsheets`).

#### Token Information

For debugging purposes the following URL can be curled, which returns the decoded token in a human readable format:

```bash
curl https://www.googleapis.com/oauth2/v1/tokeninfo\?access_token=$(gcloud auth application-default print-access-token)
```

### Dependencies
```bash
npm --prefix cloud-function install
```

### Environment Variables
Populate the `.env` file with the project specific values:

```bash
cat <<EOT >> cloud-function/.env
PGDATABASE      = "db-01"
PGHOST          = "localhost"
PGPASSWORD      = "123123"
PGPORT          = "5432"
PGUSER          = "user-01"
SPREADSHEET_ID  = "3puYYKPd2z1w3d8Zv0bS"
EOT
```

### Local Execution
Before starting the project, there should be a Postgres instance up and running on port `5432`.

```bash
npm --prefix cloud-function run watch
```

### Call Function Locally
```bash
curl localhost:8080
```

### Debug
Add breakpoints, step through your code and debug your function after running this line ([docs](https://github.com/GoogleCloudPlatform/functions-framework-nodejs/blob/master/docs/debugging.md)):

```bash
npm --prefix cloud-function run debug
```