import * as dotenv from 'dotenv'
import { HttpFunction } from '@google-cloud/functions-framework'
import { Client } from 'pg'
import { google } from 'googleapis'

dotenv.config()

export const main: HttpFunction = async (req, res) => {
    try {
        const spreadsheetId = process.env.SPREADSHEET_ID

        const auth = new google.auth.GoogleAuth({
            scopes: ['https://www.googleapis.com/auth/spreadsheets']
        })

        const authClient = await auth.getClient()

        const sheets = google.sheets({ version: 'v4', auth: authClient })

        // Clear sheet
        await sheets.spreadsheets.values.clear({
            spreadsheetId,
            range: 'Permissions!A2:Z'
        })

        // Write sheet
        const client = new Client()

        await client.connect()

        // Just a sample query. Replace it with your own.
        const result = await client.query(`
            select "id", "firstName", "lastName" from "employees";
        `)

        // The first line contains column headers, the data is inserted from line 2.
        const data = result.rows.map((row, index) => ({
            range: `A${index + 2}:C${index + 2}`,
            values: [
                [
                    row.id,
                    row.lastName,
                    row.firstName
                ]
            ]
        }))

        await client.end()

        await sheets.spreadsheets.values.batchUpdate({
            spreadsheetId,
            requestBody: {
                data,
                valueInputOption: 'RAW'
            }
        })

        res.send('Done')
    } catch (err) {
        res.status(500).send(`Error: ${(err as Error).message}`)
    }
}
