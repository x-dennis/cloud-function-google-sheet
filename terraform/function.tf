// --------------------------------------------------
// --- ACTIVATION OF GOOGLE SERVICES
// --------------------------------------------------
resource "google_project_service" "functions" {
  project = var.project
  service = "cloudfunctions.googleapis.com"

  disable_dependent_services = true
}

resource "google_project_service" "run" {
  project = var.project
  service = "run.googleapis.com"

  disable_dependent_services = true
}

resource "google_project_service" "artifact_registry" {
  project = var.project
  service = "artifactregistry.googleapis.com"

  disable_dependent_services = true
}

resource "google_project_service" "cloud_build" {
  project = var.project
  service = "cloudbuild.googleapis.com"

  disable_dependent_services = true
}

// --------------------------------------------------
// --- SERVICE ACCOUNTS
// --------------------------------------------------
resource "google_service_account" "function" {
  account_id   = "gcf-${random_id.function.hex}-sa"
  display_name = "Service account for Cloud Function accessing Google Sheets"
  project      = var.project
}

// --------------------------------------------------
// --- IAM (PROJECT)
// --------------------------------------------------
resource "google_project_iam_member" "function_sa" {
  project = var.project
  role    = "roles/cloudsql.client"
  member  = "serviceAccount:${google_service_account.function.email}"
}

// --------------------------------------------------
// --- CLOUD FUNCTION
// --------------------------------------------------
resource "google_cloudfunctions2_function" "function" {
  name        = "gcf-${random_id.function.hex}"
  location    = var.region
  description = "Cloud function updating a \"Google Sheet\" with database values"

  build_config {
    runtime     = "nodejs16"
    entry_point = "main"

    source {
      storage_source {
        bucket = google_storage_bucket.function.name
        object = google_storage_bucket_object.object.name
      }
    }
  }

  service_config {
    max_instance_count = 1
    available_memory   = "256M"
    timeout_seconds    = 60

    service_account_email = google_service_account.function.email

    environment_variables = {
      PGDATABASE      = var.db
      PGHOST          = "/cloudsql/${data.google_sql_database_instance.instance.connection_name}"
      PGPORT          = "5432"
      PGUSER          = var.db_user
      SPREADSHEET_ID  = var.spreadsheet_id
    }

    secret_environment_variables {
      key        = "PGPASSWORD"
      project_id = var.project
      secret     = google_secret_manager_secret.pg_password.secret_id
      version    = "latest"
    }
  }

  depends_on = [
    google_project_service.artifact_registry,
    google_project_service.cloud_build,
    google_project_service.run,
    google_secret_manager_secret_version.pg_password
  ]
}

// --------------------------------------------------
// --- IAM (CLOUD FUNCTION)
// --------------------------------------------------
resource "google_cloudfunctions2_function_iam_member" "job" {
  project        = var.project
  location       = var.region
  cloud_function = google_cloudfunctions2_function.function.name

  role   = "roles/cloudfunctions.invoker"
  member = "serviceAccount:${google_service_account.job.email}"
}