// --------------------------------------------------
// --- VARIABLES
// --------------------------------------------------
variable "db" {
  type        = string
  description = "Database name"
}

variable "db_instance" {
  type        = string
  description = "Name of the existing database instance"
}

variable "db_user" {
  type        = string
  description = "Database user"
}

variable "db_password" {
  type        = string
  description = "Database password"
  sensitive   = true
}

variable "project" {
  type        = string
  description = "Google Cloud project name"
}

variable "region" {
  type        = string
  description = "Region the resources should be applied to"
}

variable "spreadsheet_id" {
  type        = string
  description = "ID of the Google spreadsheet that is supposed to be populated with values"
}

// --------------------------------------------------
// --- PROVIDER
// --------------------------------------------------
provider "google" {
  project = var.project
  region  = var.region
}

// --------------------------------------------------
// --- RANDOM IDENTIFIER
// --------------------------------------------------
resource "random_id" "function" {
  byte_length = 4
}

// --------------------------------------------------
// --- OUTPUTS
// --------------------------------------------------
output "function_name" {
  value = google_cloudfunctions2_function.function.name
}

output "function_uri" {
  value = google_cloudfunctions2_function.function.service_config[0].uri
}

output "service_account_email" {
  value = google_service_account.function.email
}