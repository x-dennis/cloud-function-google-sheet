// --------------------------------------------------
// --- SERVICE ACCOUNTS
// --------------------------------------------------
resource "google_service_account" "job" {
  account_id   = "job-${random_id.function.hex}-sa"
  display_name = "Service account for scheduled Cloud Function invocation"
}

// --------------------------------------------------
// --- IAM (CLOUD RUN)
// --------------------------------------------------
// This is not documented anywhere. Usually it should be
// enough to just assign the `roles/cloudfunctions.invoker`
// role.
// Unfortunately the function rejects all requests with
// a `PERMISSION_DENIED` error until this role is added
// as well.
resource "google_cloud_run_service_iam_member" "member" {
  project   = var.project
  location  = var.region
  // The underlying Cloud Run instance has the same name
  // like the Cloud Function which triggered its creation.
  service   = google_cloudfunctions2_function.function.name

  role    = "roles/run.invoker"
  member  = "serviceAccount:${google_service_account.job.email}"

  depends_on = [
    google_cloudfunctions2_function.function
  ]
}

// --------------------------------------------------
// --- SCHEDULER
// --------------------------------------------------
resource "google_project_service" "cloud_scheduler" {
  project = var.project
  service = "cloudscheduler.googleapis.com"

  disable_dependent_services = true
}

resource "google_cloud_scheduler_job" "job" {
  name             = "job-${random_id.function.hex}"
  description      = "Scheduled execution (every 5th minute) of Cloud Function ${google_cloudfunctions2_function.function.name}"
  schedule         = "*/5 * * * *"
  time_zone        = "Europe/Berlin"
  attempt_deadline = "60s"

  retry_config {
    retry_count = 1
  }

  http_target {
    http_method = "GET"
    uri         = google_cloudfunctions2_function.function.service_config[0].uri

    oidc_token {
      service_account_email = google_service_account.job.email
    }
  }

  depends_on = [
    google_project_service.cloud_scheduler
  ]
}