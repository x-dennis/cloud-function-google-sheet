// --------------------------------------------------
// --- SECRET MANAGER AND SECRETS
// --------------------------------------------------
resource "google_project_service" "secret_manager" {
  project = var.project
  service = "secretmanager.googleapis.com"

  disable_dependent_services = true
}

resource "google_secret_manager_secret" "pg_password" {
  secret_id = "gcf-${random_id.function.hex}-pg-password"

  replication {
    user_managed {
      replicas {
        location = var.region
      }
    }
  }

  depends_on = [
    google_project_service.secret_manager
  ]
}

resource "google_secret_manager_secret_version" "pg_password" {
  secret      = google_secret_manager_secret.pg_password.name
  secret_data = var.db_password
  enabled     = true

  depends_on = [
    google_project_service.secret_manager
  ]
}

// --------------------------------------------------
// --- IAM (SECRET MANAGER)
// --------------------------------------------------
resource "google_secret_manager_secret_iam_binding" "pg_password" {
  project   = google_secret_manager_secret.pg_password.project
  secret_id = google_secret_manager_secret.pg_password.secret_id
  role      = "roles/secretmanager.secretAccessor"

  members = [
    "serviceAccount:${google_service_account.function.email}"
  ]

  depends_on = [
    google_service_account.function
  ]
}