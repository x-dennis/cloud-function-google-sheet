// --------------------------------------------------
// --- ACTIVATION OF (OTHER) GOOGLE SERVICES
// --------------------------------------------------
resource "google_project_service" "sheets" {
  project = var.project
  service = "sheets.googleapis.com"

  disable_dependent_services = true
}