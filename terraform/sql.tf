// --------------------------------------------------
// --- CLOUD SQL VALUES
// --------------------------------------------------
data "google_sql_database_instance" "instance" {
  name    = var.db_instance
  project = var.project
}