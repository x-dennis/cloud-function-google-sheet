// --------------------------------------------------
// --- CLOUD STORAGE FOR SOURCE CODE ZIP
// --------------------------------------------------
resource "google_project_service" "storage" {
  project = var.project
  service = "storage.googleapis.com"

  disable_dependent_services = true
}

// --------------------------------------------------
// --- STORAGE BUCKET
// --------------------------------------------------
resource "google_storage_bucket" "function" {
  # Every bucket name must be globally unique
  name      = "${var.project}-gcf-${random_id.function.hex}"
  location  = var.region

  uniform_bucket_level_access = true
}

// --------------------------------------------------
// --- BUILD ARCHIVE
// --------------------------------------------------
data "archive_file" "function" {
  type        = "zip"
  source_dir  = "${path.root}/../cloud-function/build"
  output_path = "${path.root}/build/function.zip"
}

// --------------------------------------------------
// --- STORE ZIPPED SOURCE CODE IN BUCKET
// --------------------------------------------------
resource "google_storage_bucket_object" "object" {
  name   = "${data.archive_file.function.output_md5}.zip"
  bucket = google_storage_bucket.function.name
  source = data.archive_file.function.output_path
}